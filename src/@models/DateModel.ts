interface DateFormatInterface {
  month: () => string;
  day: string;
  year: string;
}

export { DateFormatInterface };
