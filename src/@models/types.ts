type StatusType = 'completed' | 'complete' | 'to_be_aired' | 'tba' | 'upcoming';
type SearchType = 'anime' | 'manga' | 'person' | 'character';
type AnimeStatus = 'airing' | StatusType;
type MangaStatus = 'publishing' | StatusType;

export { StatusType, SearchType, AnimeStatus, MangaStatus };
