import { SearchType, AnimeStatus, MangaStatus } from './types';

//created a generic type for anime,manage,etc.
interface GenericSearchTypeInterface {
  mal_id: string;
  url: string;
  image_url: string;
  title: string;
  synopsis: string;
  type: string;
  score: number;
  start_date: string;
  end_date: string;
  members: number;
}

interface AnimeInterface extends GenericSearchTypeInterface {
  airing: boolean;
  episodes: number;
  rated: string;
}
interface MangaInterface extends GenericSearchTypeInterface {
  chapters: number;
  publishing: boolean;
  volumes: number;
}

interface SearchInterface {
  query: string;
  type: SearchType;
  status: {
    anime: AnimeStatus;
    manga: MangaStatus;
  };
}

export { AnimeInterface, MangaInterface, SearchInterface };
