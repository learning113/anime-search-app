import { InjectionKey } from 'vue';
import { store } from 'quasar/wrappers';
import { createStore, useStore as baseUseStore, Store } from 'vuex';
import { AnimeInterface } from '../@models/SearchModel';

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export interface StateInterface {
  animeList: AnimeInterface[];
}

export default store(function (/* { ssrContext } */) {
  const Store = createStore<StateInterface>({
    state: {
      animeList: [],
    },

    mutations: {
      SET_ANIME_LIST: (state: StateInterface, animeList: AnimeInterface[]) => {
        state.animeList = animeList;
      },
    },

    actions: {},

    getters: {
      animeList(state: StateInterface): AnimeInterface[] {
        return state.animeList;
      },
    },

    modules: {},

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: !!process.env.DEBUGGING,
  });

  return Store;
});

// define injection key
export const key: InjectionKey<Store<StateInterface>> = Symbol();

declare module '@vue/runtime-core' {
  // provide typings for `this.$store`
  interface ComponentCustomProperties {
    $store: Store<StateInterface>;
  }
}

export function useStore() {
  return baseUseStore(key);
}
